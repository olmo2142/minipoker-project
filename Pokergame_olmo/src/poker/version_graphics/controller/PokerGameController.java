package poker.version_graphics.controller;

import java.io.File;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;
import poker.version_graphics.model.Card;
import poker.version_graphics.model.DeckOfCards;
import poker.version_graphics.model.Player;
import poker.version_graphics.model.PokerGameModel;
import poker.version_graphics.view.ControlArea;
import poker.version_graphics.view.PlayerPane;
import poker.version_graphics.view.PokerGameStartPage;
import poker.version_graphics.view.PokerGameView;

public class PokerGameController {
    private PokerGameModel model;
    private PokerGameView view;
    private PokerGameStartPage startview;
    private static Stage stage;

    public PokerGameController(PokerGameModel model, PokerGameStartPage startview) {
	this.model = model;
	this.view = new PokerGameView(model, this);
	this.startview = startview;

	startview.getBtnGoPlay().setOnAction(e -> goPlay());
	startview.getBtnNewGame().setOnAction(e -> newGame());
	startview.getBtnExit().setOnAction(e -> System.exit(0));
	startview.getBtnAddPlayer().setOnAction(e -> addPlayer());
	view.getGoStartWindowButton().setOnAction(e -> goStartWindow());
	view.getShuffleButton().setOnAction(e -> shuffle());
	view.getDealButton().setOnAction(e -> deal());
    }

    public void setStage(Stage stage2) {
	stage = stage2;
    }

    public void showStage() {
	stage.show();
    }

    // öffnet Spielfenster und mischt karten neu durch
    private void goPlay() {
	view.updateGameView();
	shuffle();
	ControlArea.updatelblWinner("los gehts...");
	stage = PokerGameView.getStage();
	showStage();
	PokerGameStartPage.closeWindow();
    }

    private void addPlayer() {
	model.addPlayer();
	startview.updatePlayerDislpay();
    }

    private void newGame() {
	model.newGame();
	startview.updatePlayerDislpay();
    }

    private void goStartWindow() {
	startview.updatePlayerDislpay();
	stage = PokerGameStartPage.getStage();
	showStage();
	view.closeWindow();
    }

    /**
     * Remove all cards from players hands, and shuffle the deck
     * 
     */
    private void shuffle() {
	for (int i = 0; i < PokerGameModel.num_players; i++) {
	    Player p = model.getPlayer(i);
	    p.discardHand();
	    PlayerPane pp = view.getPlayerPane(i);
	    pp.updatePlayerDisplay();
	}

	// Play sound
	Media sound = new Media(new File("shuffle.mp3").toURI().toString());
	MediaPlayer mp = new MediaPlayer(sound);
	mp.play();

	ControlArea.updatelblWinner("los gehts...");
	model.getDeck().shuffle();
    }

    /**
     * Deal each player five cards, then evaluate the two hands
     */
    private void deal() {
	int cardsRequired = PokerGameModel.num_players * Player.HAND_SIZE;
	DeckOfCards deck = model.getDeck();
	if (cardsRequired <= deck.getCardsRemaining()) {
	    for (int i = 0; i < PokerGameModel.num_players; i++) {
		Player p = model.getPlayer(i);
		p.discardHand();
		for (int j = 0; j < Player.HAND_SIZE; j++) {
		    Card card = deck.dealCard();
		    p.addCard(card);
		}
		p.evaluateHand();
		PlayerPane pp = view.getPlayerPane(i);
		pp.updatePlayerDisplay();

	    }

	    // Play sound
	    Media sound = new Media(new File("deal.mp3").toURI().toString());
	    MediaPlayer mp = new MediaPlayer(sound);
	    mp.play();

	    model.evaluateWinner();

	} else {
	    Alert alert = new Alert(AlertType.ERROR, "Not enough cards - shuffle first");
	    alert.showAndWait();
	}
	if (PokerGameModel.winnerCard == null) {
	    ControlArea.updatelblWinner(
		    "ID: " + PokerGameModel.roundWinner.getPlayerID() + " gewinnt mit Hand: " + PokerGameModel.winnerHandType);
	} else {
	    ControlArea.updatelblWinner(
		    "ID: " + PokerGameModel.roundWinner.getPlayerID() + " gewinnt mit Karte: " + PokerGameModel.winnerCard);
	}
    }

}
