package poker.version_graphics;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import poker.version_graphics.controller.PokerGameController;
import poker.version_graphics.model.PokerGameModel;
import poker.version_graphics.view.PokerGameStartPage;
import poker.version_graphics.view.PokerGameView;

public class PokerGame extends Application {
	//public static int num_players = 0;
	public final static int MIN_PLAYERS = 2;
	public final static int MAX_PLAYERS = 8;
	PokerGameModel model;
	PokerGameController controller;
	PokerGameStartPage startview;
	public static Stage primaryStage;

	
    public static void main(String[] args) {
        launch();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
	PokerGame.primaryStage = primaryStage;
 
	// Create and initialize the MVC components
    	model = new PokerGameModel();	
    	startview = new PokerGameStartPage(primaryStage, model);
    	controller = new PokerGameController(model, startview);
    	controller.setStage(PokerGameStartPage.getStage());
    	controller.showStage();
    	
    }
    
}
