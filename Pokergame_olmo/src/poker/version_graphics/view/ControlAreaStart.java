package poker.version_graphics.view;

import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;


public class ControlAreaStart extends HBox{
    private DeckLabel lblDeck = new DeckLabel();
    private DeckLabel lblWinner = new DeckLabel();
    private Region spacer = new Region(); // Empty spacer
    Button btnAddPlayer = new Button("Add Player");
    Button btnNewGame = new Button("NewGame");
    Button btnGoPlay = new Button("Start Game!");
    Button btnExit = new Button("Exit");

    public ControlAreaStart() {
    	super(); // Always call super-constructor first !!
    	
    	this.getChildren().addAll(lblDeck, lblWinner,  btnAddPlayer, btnNewGame, btnGoPlay, btnExit);

       // HBox.setHgrow(spacer, Priority.ALWAYS); // Use region to absorb resizing
        this.setId("controlArea"); // Unique ID in the CSS
    }
    
}
