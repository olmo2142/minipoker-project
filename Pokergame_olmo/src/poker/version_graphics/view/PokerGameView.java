package poker.version_graphics.view;

import java.util.ArrayList;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import poker.version_graphics.PokerGame;
import poker.version_graphics.controller.PokerGameController;
import poker.version_graphics.model.Player;
import poker.version_graphics.model.PokerGameModel;

public class PokerGameView {

    private ScrollPane playfield;
    private TilePane players;
    private ControlArea controls;
    private PokerGameModel model;
    private PokerGameController controller;
    private static Stage stage;

    public PokerGameView(PokerGameModel model, PokerGameController controller) {
	this.model = model;
	this.controller = controller;
	controls = new ControlArea();

    }

    public void updateGameView() {
	// Create all of the player panes we need, and put them into an HBox
	players = new TilePane();


	controls.linkDeck(model.getDeck()); // link DeckLabel to DeckOfCards in the logic
	playfield = new ScrollPane();
	{
	    playfield.setFitToWidth(true);
	    playfield.setContent(players);

	}

	// Put players and controls into a BorderPane
	BorderPane root = new BorderPane();
	root.setCenter(playfield);
	root.setBottom(controls);

	// Create the scene using our layout; then display it
	Scene scene2 = new Scene(root);
	scene2.getStylesheets().add(getClass().getResource("poker.css").toExternalForm());
	Stage stage2 = new Stage();
	stage2.setTitle("Poker Miniproject");
	stage2.setScene(scene2);
	stage = stage2;
	for (int i = 0; i < PokerGameModel.num_players; i++) {
	    PlayerPane pp = new PlayerPane();
	    pp.setPlayer(model.getPlayer(i)); // link to player object in the logic
	    players.getChildren().add(pp);
	    players.setPrefColumns((int) Math.sqrt(PokerGameModel.num_players));
	}

    }

    public static Stage getStage() {
	return stage;
    }

    public void closeWindow() {
	stage.close();
    }

    public PlayerPane getPlayerPane(int i) {
	return (PlayerPane) players.getChildren().get(i);
    }

    public Button getShuffleButton() {
	return controls.btnShuffle;
    }

    public Button getDealButton() {
	return controls.btnDeal;
    }

    public Button getGoStartWindowButton() {
	return controls.btnGoStartWindow;
    }
}
