package poker.version_graphics.view;

import java.util.ArrayList;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import poker.version_graphics.PokerGame;
import poker.version_graphics.controller.PokerGameController;
import poker.version_graphics.model.Player;
import poker.version_graphics.model.PokerGameModel;

public class PokerGameStartPage {

    private PokerGameModel model;
    private ControlAreaStart controls;
    private PokerGameController controller;
    private static Stage stage1;

    public PokerGameStartPage(Stage primaryStage, PokerGameModel model) {
	this.model = model;
	stage1 = primaryStage;
	controls = new ControlAreaStart();

	updatePlayerDislpay();

    }

    public void updatePlayerDislpay() {

	// Create the control area
	BorderPane root = new BorderPane();
	root.setBottom(controls);

	HBox centerBox = new HBox(20);
	root.setCenter(centerBox);

	Scene scene = new Scene(root, 800, 600);
	scene.getStylesheets().add(getClass().getResource("StartPage.css").toExternalForm());
	VBox playerList = new VBox();

	ArrayList<Player> PlayersOnGame = model.getPlayersOnGame();

	for (Player p : PlayersOnGame) {
	    HBox playerBox = new HBox();
	    Label playerNumber = new Label();
	    playerNumber.setText("Spieler Nr: " + (p.getPlayerNumber()+1));
	    Label playerName = new Label();
	    playerName.setText("" + p.getPlayerName());
	    Label winsPerGame = new Label();
	    winsPerGame.setText("Wins: "+ p.getPlayerScore());
	    playerBox.getChildren().addAll(playerNumber, playerName, winsPerGame);
	    playerList.getChildren().add(playerBox);

	}
	VBox players = new VBox();
	players.getChildren().addAll(new Label("Players"), playerList);

	
	centerBox.getChildren().addAll(players);
	stage1.setTitle("PokerGame-Start");
	stage1.setScene(scene);
    }

    public Button getBtnGoPlay() {
	return controls.btnGoPlay;
    }

    public Button getBtnExit() {
	return controls.btnExit;
    }

    public Button getBtnAddPlayer() {
	return controls.btnAddPlayer;
    }
    
    public Button getBtnNewGame() {
	return controls.btnNewGame;
    }

    public static Stage getStage() {
	return stage1;
    }

    public static void closeWindow() {
	stage1.close();
    }

}
