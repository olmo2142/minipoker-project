package poker.version_graphics.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import poker.version_graphics.PokerGame;
import poker.version_graphics.model.Card.Rank;
import poker.version_graphics.model.Hand.HandType;

public class PokerGameModel {
    private final ArrayList<Player> players = new ArrayList<>();
    private final ArrayList<Player> playersOnGame = new ArrayList<>();
    private DeckOfCards deck;
    public static int counterPlayerID = 0;
    public static int num_players = 0;
    public static Player roundWinner;
    public static Rank winnerCard;
    public static HandType winnerHandType;

    public PokerGameModel() {
	for (int i = 0; i < PokerGame.MIN_PLAYERS; i++) {
	    addPlayer();
	}
	deck = new DeckOfCards();
    }

    public Player getPlayer(int PlayerNumber) {
	Player pl = new Player();
	for (Player p : playersOnGame)
	    if (p.getPlayerNumber() == PlayerNumber)
		pl = p;
	return pl;
    }

    public void setPlayersOnGame() {
	this.playersOnGame.clear();
	int playerNumber = 0;
	for (Player p : players)
	    if (p.getOnGame() == true) {
		p.setPlayerNumber(playerNumber);
		playerNumber++;
		this.playersOnGame.add(p);
	    }
    }

    public ArrayList getPlayersOnGame() {
	return playersOnGame;
    }

    public DeckOfCards getDeck() {
	return deck;
    }

    public void addPlayer() {
	if (num_players < PokerGame.MAX_PLAYERS) {
	    Player pl = new Player();
	    counterPlayerID++;
	    pl.setPlayerID(counterPlayerID);
	    pl.setOnGame(true);
	    players.add(pl);
	    num_players++;
	    setPlayersOnGame();
	} else {
	    Alert alert = new Alert(AlertType.ERROR, "Maximale Spieleranzahl von 8 darf nicht überschritten werden");
	    alert.showAndWait();
	}
    }

    public void newGame() {
	Iterator<Player> iter = players.iterator();
	while (iter.hasNext()) {
	    Player pl = iter.next();
	    if (pl.getOnGame() == true) {
		pl.setOnGame(false);
		num_players--;
	    }
	}
	for (int i = 0; i < PokerGame.MIN_PLAYERS; i++) {
	    addPlayer();
	}
    }

    public void evaluateWinner() {
	Collections.sort(playersOnGame);
	Collections.reverse(playersOnGame);
	winnerHandType = playersOnGame.get(0).getHandType();

	if (playersOnGame.get(0).getHandType().equals(playersOnGame.get(1).getHandType())) {
	    ArrayList<Player> firstSearch = new ArrayList();
	    for (Player p : playersOnGame)
		if (p.getHandType().equals(playersOnGame.get(0).getHandType()))
		    firstSearch.add(p);

	    switch (winnerHandType) {

	    case HighCard:
		if (firstCardsEvaluate(firstSearch) == false) {
		    roundWinner = null;
		}

		break;

	    case OnePair:
		if (secondCardsEvaluate(firstSearch) == false) {
		    if (thirdCardsEvaluate(firstSearch) == false) {
			roundWinner = null;
		    }
		}

		break;

	    case TwoPair:
		if (firstCardsEvaluate(firstSearch) == false) {
		    if (secondCardsEvaluate(firstSearch) == false) {
			if (thirdCardsEvaluate(firstSearch) == false) {
			    roundWinner = null;
			}
		    }
		}

		break;

	    case ThreeOfAKind:
		if (firstCardsEvaluate(firstSearch) == false) {
		    if (secondCardsEvaluate(firstSearch) == false) {
			roundWinner = null;
		    }
		}

		break;

	    case Straight:
		if (firstCardsEvaluate(firstSearch) == false) {
		    roundWinner = null;
		}

		break;

	    case Flush:
		if (firstCardsEvaluate(firstSearch) == false) {
		    roundWinner = null;
		}

		break;

	    case FullHouse:
		if (firstCardsEvaluate(firstSearch) == false) {
		    if (secondCardsEvaluate(firstSearch) == false) {
			roundWinner = null;
		    }
		}

		break;

	    case FourOfAKind:
		if (firstCardsEvaluate(firstSearch) == false) {
		    if (secondCardsEvaluate(firstSearch) == false) {
			roundWinner = null;
		    }
		}

		break;

	    case StraightFlush:
		if (firstCardsEvaluate(firstSearch) == false) {
		    roundWinner = null;
		}

		break;
	    }
	}
	
	// wenn nur ein Spieler die höchste Hand hat, ist der der Gewinner
	else {
	    winnerCard = null;
	    roundWinner = playersOnGame.get(0);
	    winnerHandType = roundWinner.getHandType();
	}
	// fügt Gewinner Punkt hinzu
	if (roundWinner != null)
	    roundWinner.setPlayerScore(1);
    }
    
    // höchste Karte wird gesucht, die nur einmal im Spiel pro Spieler vorkommt
    
    	// Suchlauf der ersten Kartenserie der Spieler nach der höchsten Karte
    private boolean firstCardsEvaluate(ArrayList<Player> firstSearch) {
	int player = -1;
	boolean found = false;
	int count2 = 0;
	int card = -1;
	for (int i = 12; i >= 0 && !found; i--) {
	    for (int j = 0; j < firstSearch.size(); j++) {
		if (firstSearch.get(j).getHand().getFCV().contains(i)) {
		    count2++;
		    player = j;
		    card = i;
		}
	    }
	    if (count2 == 1) {
		found = true;
		roundWinner = firstSearch.get(player);
		winnerCard = Card.Rank.values()[card];
		count2 = 0;
	    }
	}
	return found;

    }
    	// Suchlauf in der zweiten Kartenkategorie der Spieler nach der höchsten Karte
    private boolean secondCardsEvaluate(ArrayList<Player> firstSearch) {
	int player = -1;
	boolean found = false;
	int count2 = 0;
	int card = -1;
	for (int i = 12; i >= 0 && !found; i--) {
	    for (int j = 0; j < firstSearch.size(); j++) {
		if (firstSearch.get(j).getHand().getSCV().contains(i)) {
		    count2++;
		    player = j;
		    card = i;
		}
	    }
	    if (count2 == 1) {
		found = true;
		roundWinner = firstSearch.get(player);
		winnerCard = Card.Rank.values()[card];
		count2 = 0;
	    }
	}
	return found;

    }
    	// Suchlauf in der dritten Kartenkategorie der Spieler nach der höchsten Karte
    private boolean thirdCardsEvaluate(ArrayList<Player> firstSearch) {
	int player = -1;
	boolean found = false;
	int count2 = 0;
	int card = -1;
	for (int i = 12; i >= 0 && !found; i--) {
	    for (int j = 0; j < firstSearch.size(); j++) {
		if (firstSearch.get(j).getHand().getTCV().contains(i)) {
		    count2++;
		    player = j;
		    card = i;
		}
	    }
	    if (count2 == 1) {
		found = true;
		roundWinner = firstSearch.get(player);
		winnerCard = Card.Rank.values()[card];
		count2 = 0;
	    }
	}
	return found;
    }

}
