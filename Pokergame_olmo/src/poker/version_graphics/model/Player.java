package poker.version_graphics.model;

import java.util.ArrayList;
import poker.version_graphics.model.Hand.HandType;

public class Player implements Comparable<Player> {
    // public static int counterPlayerID = 0;
    public static final int HAND_SIZE = 5;

    private int playerID; // This is the ID
    private final ArrayList<Card> cards = new ArrayList<>();  
    private Hand hand;
    private HandType handType;
    private int playerScore;
    private boolean onGame;
    private int playerNumber;

    public Player() {
	this.onGame = false;
	this.playerScore = 0;
	this.playerNumber = 0;
	this.hand = new Hand();
	this.handType = null;
    }

    public void setPlayerID(int id) {
	playerID = id;
    }

    public int getPlayerID() {
	return playerID;
    }

    public String getPlayerName() {
	return "Player-ID: " + playerID;
    }

    public ArrayList<Card> getCards() {
	return cards;
    }

    public void addCard(Card card) {
	if (cards.size() < HAND_SIZE)
	    cards.add(card);
    }

    public void discardHand() {
	cards.clear();
	handType = null;
    }

    public int getNumCards() {
	return cards.size();
    }
    public HandType getHandType() {
	return handType;
    }
   public Hand getHand() {
       return hand;
   }
    
    

    /**
     * If the hand has not been evaluated, but does have all cards, then evaluate
     * it.
     */
    public HandType evaluateHand() {
	if (handType == null && cards.size() == HAND_SIZE) {
	    this.hand.evaluateHand(cards);
	    this.handType = this.hand.getHandType();
	}
	return handType;
    }

    public void setOnGame(boolean onGame) {
	this.onGame = onGame;
    }

    public boolean getOnGame() {
	return onGame;
    }

    public void removePlayer() {
	this.onGame = false;
    }

    public void setPlayerNumber(int playerNumber) {
	this.playerNumber = playerNumber;
    }

    public int getPlayerNumber() {
	return playerNumber;
    }

    public int getPlayerScore() {
	return playerScore;
    }

    public void setPlayerScore(int score) {
	this.playerScore += score;
    }
    public String toString() {
	return "PlNr: "+ this.playerNumber +"|ID: "+this.playerID+"|Hand: "+this.handType + 
		this.hand.getFCV()+this.hand.getSCV()+this.hand.getTCV()+"\n";
    }

    /**
     * Hands are compared, based on the evaluation they have.
     */
    @Override
    public int compareTo(Player o) {
	return handType.compareTo(o.handType);
    }

}
