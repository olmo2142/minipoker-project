package poker.version_graphics.model;

import java.util.ArrayList;
import java.util.Collections;

public class Hand {

    /**
     * Determine the value of this hand. Note that this does not account for any
     * tie-breaking
     */
    public enum HandType {
    HighCard, OnePair, TwoPair, ThreeOfAKind, Straight, Flush, FullHouse, FourOfAKind, StraightFlush
    }

    private ArrayList<Integer> firstCardValue;
    private ArrayList<Integer> secondCardValue;
    private ArrayList<Integer> thirdCardValue;
    private HandType hand;

    public Hand() {
	this.firstCardValue = new ArrayList<Integer>();
	this.secondCardValue = new ArrayList<Integer>();
	this.thirdCardValue = new ArrayList<Integer>();
	this.hand = null;
    }

    public void evaluateHand(ArrayList<Card> cards) {

	this.firstCardValue.clear();
	this.secondCardValue.clear();
	this.thirdCardValue.clear();

	isHighCard(cards);
	this.hand = HandType.HighCard;

	if (isOnePair(cards))
	    this.hand = HandType.OnePair;
	if (isTwoPair(cards))
	    this.hand = HandType.TwoPair;
	if (isThreeOfAKind(cards))
	    this.hand = HandType.ThreeOfAKind;
	if (isStraight(cards))
	    this.hand = HandType.Straight;
	if (isFlush(cards))
	    this.hand = HandType.Flush;
	if (isFullHouse(cards))
	    this.hand = HandType.FullHouse;
	if (isFourOfAKind(cards))
	    this.hand = HandType.FourOfAKind;
	if (isStraightFlush(cards))
	    this.hand = HandType.StraightFlush;
    }

    public void valueSort() {
	Collections.sort(firstCardValue);
	Collections.sort(secondCardValue);
	Collections.sort(secondCardValue);
	Collections.reverse(firstCardValue);
	Collections.reverse(secondCardValue);
	Collections.reverse(thirdCardValue);
    }

    public HandType getHandType() {
	return hand;
    }

    // Durchsucht nach einem Paar, das mit dem höchsten Wert wird verwendet
    protected boolean isOnePair(ArrayList<Card> cards) {
	ArrayList<Card> clonedCards = (ArrayList<Card>) cards.clone();
	ArrayList<Integer> pairCards = new ArrayList<Integer>();
	ArrayList<Integer> otherCards = new ArrayList<Integer>();
	Collections.sort(clonedCards);
	boolean found = false;
	for (int i = clonedCards.size() - 1; i > 0 && !found; i--) {

	    if (clonedCards.get(i).getRank() == cards.get(i - 1).getRank()) {
		found = true;
		pairCards.add(cards.get(i).getOrdinal());
		clonedCards.remove(i);
		clonedCards.remove(i - 1);
		for (Card c : clonedCards)
		    otherCards.add(c.getOrdinal());
	    }
	}
	if (found) {
	    this.secondCardValue = pairCards;
	    this.thirdCardValue = otherCards;
	}
	valueSort();
	return found;
    }

    // Durchsucht nach zwei Paaren
    protected boolean isTwoPair(ArrayList<Card> cards) {
	// Clone the cards, because we will be altering the list
	ArrayList<Card> clonedCards = (ArrayList<Card>) cards.clone();
	ArrayList<Integer> firstPairCards = new ArrayList<Integer>();
	// Find the first pair; if found, remove the cards from the list
	boolean firstPairFound = false;
	boolean twoPairFound = false;
	for (int i = clonedCards.size() - 1; i > 0 && !firstPairFound; i--) {
	    for (int j = i - 1; j >= 0 && !firstPairFound; j--) {
		if (clonedCards.get(i).getRank() == clonedCards.get(j).getRank()) {
		    firstPairFound = true;
		    firstPairCards.add(clonedCards.get(i).getOrdinal());
		    clonedCards.remove(i); // Remove the later card
		    clonedCards.remove(j); // Before the earlier one
		}
	    }
	}
	// If a first pair was found, see if there is a second pair
	if (isOnePair(clonedCards)) {
	    twoPairFound = true;
	    this.firstCardValue = firstPairCards;
	}
	valueSort();
	return twoPairFound;
    }

    // Durchsucht nach 3 Karten nach gleichem Rank
    protected boolean isThreeOfAKind(ArrayList<Card> cards) {
	Collections.sort(cards);
	boolean threeFound = false;
	ArrayList<Integer> threeCards = new ArrayList<Integer>();
	ArrayList<Integer> twoCards = new ArrayList<Integer>();
	int rankLow = 0;
	int rankHigh = 0;
	for (int i = 0; i < cards.size() - 3; i++) {
	    if (cards.get(i).getRank() == cards.get(i + 1).getRank()) {
		rankLow++;
	    }
	}

	if (rankLow == 2) {
	    threeFound = true;
	    threeCards.add(cards.get(0).getOrdinal());
	    twoCards.add(cards.get(3).getOrdinal());
	    twoCards.add(cards.get(4).getOrdinal());
	    this.firstCardValue = threeCards;
	    this.secondCardValue = twoCards;
	}

	Collections.reverse(cards);
	for (int i = 0; i < cards.size() - 3; i++) {
	    if (cards.get(i).getRank() == cards.get(i + 1).getRank()) {
		rankHigh++;
	    }
	}

	if (rankHigh == 2) {
	    threeFound = true;
	    threeCards.add(cards.get(0).getOrdinal());
	    twoCards.add(cards.get(3).getOrdinal());
	    twoCards.add(cards.get(4).getOrdinal());
	    this.firstCardValue = threeCards;
	    this.secondCardValue = twoCards;
	}
	valueSort();
	return threeFound;

    }

    // 5 aufeinanderfolgende Karten/Rank
    protected boolean isStraight(ArrayList<Card> cards) {
	ArrayList<Integer> straightCards = new ArrayList<Integer>();
	boolean stright = false;
	Collections.sort(cards);
	for (int i = 0; i < 10; i++) {
	    if (cards.get(0).getRank().ordinal() == i && cards.get(1).getRank().ordinal() == i + 1
		    && cards.get(2).getRank().ordinal() == i + 2 && cards.get(3).getRank().ordinal() == i + 3
		    && cards.get(4).getRank().ordinal() == i + 4) {
		stright = true;
		for (Card c : cards)
		    straightCards.add(c.getRank().ordinal());
		this.firstCardValue = straightCards;
	    }
	}
	return stright;
    }

    // prüft ob alle Karten gleiche Suit haben
    protected boolean isFlush(ArrayList<Card> cards) {
	Collections.sort(cards);
	boolean flush = true;
	for (int i = 0; i < cards.size() - 1; i++) {
	    if (cards.get(i).getSuit() != cards.get(i + 1).getSuit())
		flush = false;
	}
	valueSort();
	return flush;
    }

    // Prüfung 3 Karten gleicher Art/Rank & 1 Paar
    protected boolean isFullHouse(ArrayList<Card> cards) {
	ArrayList<Integer> trioCards = new ArrayList<Integer>();
	ArrayList<Card> falseCards = new ArrayList<Card>();
	Collections.sort(cards);
	boolean trioFound = false;
	boolean fullHouse = false;
	if (cards.get(0).getRank() == cards.get(1).getRank() && cards.get(1).getRank() == cards.get(2).getRank()) {
	    trioFound = true;
	    trioCards.add(cards.get(0).getOrdinal());
	    falseCards.add(cards.get(3));
	    falseCards.add(cards.get(4));
	} else if (cards.get(2).getRank() == cards.get(3).getRank()
		&& cards.get(3).getRank() == cards.get(4).getRank()) {
	    trioFound = true;
	    trioCards.add(cards.get(2).getOrdinal());
	    falseCards.add(cards.get(0));
	    falseCards.add(cards.get(1));
	}
	if (isOnePair(falseCards) && trioFound) {
	    fullHouse = true;
	    this.firstCardValue = trioCards;
	}
	valueSort();
	return fullHouse;
    }

    // Prüfung 4 Karten gleicher Art
    protected boolean isFourOfAKind(ArrayList<Card> cards) {
	ArrayList<Integer> fourCards = new ArrayList<Integer>();
	ArrayList<Integer> oneCards = new ArrayList<Integer>();
	Collections.sort(cards);
	boolean fourFound = false;
	int rankLow = 0;
	int rankHigh = 0;

	for (int i = 0; i < cards.size() - 2; i++) {
	    if (cards.get(i).getRank() == cards.get(i + 1).getRank()) {
		rankLow++;
	    }
	}
	for (int i = cards.size() - 1; i > 0; i--) {

	    if (cards.get(i).getRank() == cards.get(i - 1).getRank()) {
		rankHigh++;
	    }
	}
	if (rankLow == 3) {
	    fourFound = true;
	    fourCards.add(cards.get(0).getOrdinal());
	    oneCards.add(cards.get(4).getOrdinal());
	    this.firstCardValue = fourCards;
	    this.secondCardValue = oneCards;
	}
	if (rankHigh == 3) {
	    fourFound = true;
	    fourCards.add(cards.get(4).getOrdinal());
	    oneCards.add(cards.get(0).getOrdinal());
	    this.firstCardValue = fourCards;
	    this.secondCardValue = oneCards;
	}
	valueSort();
	return fourFound;
    }

    protected boolean isStraightFlush(ArrayList<Card> cards) {

	boolean found = false;
	if (isFlush(cards) && isStraight(cards)) {
	    found = true;
	    isHighCard(cards);
	}
	valueSort();
	return found;
    }

    protected void isHighCard(ArrayList<Card> cards) {
	ArrayList<Integer> cardsInt = new ArrayList<Integer>();
	Collections.sort(cards);
	for (Card c : cards)
	    cardsInt.add(c.getRank().ordinal());
	this.firstCardValue = cardsInt;
	valueSort();
    }

    public ArrayList<Integer> getFCV() {
	return this.firstCardValue;
    }

    public ArrayList<Integer> getSCV() {
	return this.secondCardValue;
    }

    public ArrayList<Integer> getTCV() {
	return this.thirdCardValue;
    }
}
